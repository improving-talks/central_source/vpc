## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| azs | n/a | `list(string)` | n/a | yes |
| cidr | CIDR Range for the VPC | `string` | n/a | yes |
| database\_subnets | n/a | `list(string)` | n/a | yes |
| environment | Name of the environment | `string` | n/a | yes |
| name | n/a | `string` | n/a | yes |
| private\_subnets | n/a | `list(string)` | n/a | yes |
| public\_subnets | n/a | `list(string)` | n/a | yes |
| tags | Tags to apply to resource created from this module | `map(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| database\_subnet\_ids | n/a |
| id | n/a |
| private\_subnet\_ids | n/a |
| public\_subnet\_ids | n/a |

