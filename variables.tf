variable "environment" {
  type        = string
  description = "Name of the environment"
}

variable "name" {
  type = string
}

variable "azs" {
  type = list(string)
}

variable "cidr" {
  type        = string
  description = "CIDR Range for the VPC"
}

variable "private_subnets" {
  type = list(string)
}

variable "public_subnets" {
  type = list(string)
}

variable "database_subnets" {
  type = list(string)
}

variable "tags" {
  description = "Tags to apply to resource created from this module"
  type        = map(string)
}

locals {
  name = "${var.name}-${var.environment}"
  tags = merge(var.tags, {
    service = "network"
  })
}