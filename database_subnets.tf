#################
# database subnet
#################
resource "aws_subnet" "database" {
  count = length(var.database_subnets) > 0 ? length(var.database_subnets) : 0

  vpc_id            = aws_vpc.this.id
  cidr_block        = var.database_subnets[count.index]
  availability_zone = element(var.azs, count.index)

  tags = merge(
    {
      Name        = "${local.name}-database-${var.azs[count.index]}",
      subnet_type = "database"
    },
    local.tags,
  )
}

#################
# database routes
#################
resource "aws_route_table" "database" {
  vpc_id = aws_vpc.this.id

  tags = merge(
    {
      Name = "${local.name}-database",
    },
    local.tags,
  )

  lifecycle {
    # When attaching VPN gateways it is common to define aws_vpn_gateway_route_propagation
    # resources that manipulate the attributes of the routing table (typically for the database subnets)
    ignore_changes = [propagating_vgws]
  }
}

##########################
# Route table association
##########################
resource "aws_route_table_association" "database" {
  count = length(var.database_subnets) > 0 ? length(var.database_subnets) : 0

  subnet_id      = aws_subnet.database[count.index].id
  route_table_id = aws_route_table.database.id
}